﻿-- Creacion de base de datos correspondiente al ejercicio 1 de la hoja 1
-- Tiene 5 tablas

DROP DATABASE IF EXISTS b20190529;
CREATE DATABASE b20190529;
USE b20190529;

/*
  Creacion de la tabla clientes 
*/
CREATE TABLE clientes(
  dni int AUTO_INCREMENT,
  nombre varchar (100),
  apellidos varchar (100),
  fecha_nac date,
  tfno varchar (12),
  PRIMARY KEY (dni)
);

/*
  Creacion de la tabla productos 
*/
CREATE TABLE productos(
  codigo int AUTO_INCREMENT,
  nombre varchar (100),
  precio float,
  PRIMARY KEY (codigo)
);

/*
  Creacion de la tabla proveedores 
*/
CREATE TABLE proveedores(
  nif int AUTO_INCREMENT,
  nombre varchar (100),
  direccion varchar (100),
  PRIMARY KEY (nif)
);

/*
  Creacion de la tabla compra en al que se relacionan  los clientes con los productos que compran 
*/
CREATE TABLE compra(
  dniCliente int,
  codProducto int,
  PRIMARY KEY (dniCliente, codProducto),
  CONSTRAINT fkCompraClientes FOREIGN KEY (dniCliente) REFERENCES clientes(dni),
  CONSTRAINT fkCompraProductos FOREIGN KEY (codProducto) REFERENCES productos(codigo)                                                         
);

/*
  Creacion de la tabla suministra en la que se realinan los proveedores con los productos que suministran
*/
CREATE TABLE suministra(
  codProducto int,
  nifProveedor int,
  PRIMARY KEY (codProducto, nifProveedor),
  UNIQUE KEY (codProducto),
  CONSTRAINT fkSuministraProducto FOREIGN KEY (codProducto) REFERENCES productos(codigo),
  CONSTRAINT fkSuministraProveedor FOREIGN KEY (nifProveedor) REFERENCES proveedores(nif)
);

-- insertar datos en las tablas

-- insertar en tabla clientes
INSERT INTO clientes (nombre, apellidos, fecha_nac, tfno) VALUES
('Juan', 'Sainz', '1995/25/4', '658741256'),
('Laura', 'Pérez', '1993/2/8', '632548965');

-- insertar en tabla productos
INSERT INTO productos (nombre, precio) VALUES 
('mesa', 150),
('silla', 50);

-- insertar en tabla proveedores
INSERT INTO proveedores (nombre, direccion) VALUES 
('Ikea', 'Barakaldo');

-- insertar en tablas compra
INSERT INTO compra (dniCliente, codProducto) VALUES 
(1, 2),
(1, 1),
(2, 1);

-- insertar en tabla suministra
INSERT INTO suministra (codProducto, nifProveedor) VALUES 
(1, 1),
(2, 1);
-- no lo permite porque no se puede suministrar un producto por varios proveedores (1, 2);